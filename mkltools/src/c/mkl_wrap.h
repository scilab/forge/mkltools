/* ========================================================================= */
// Copyright (C) 2010 - DIGITEO - Allan CORNET
/* ========================================================================= */
#ifndef __MKL_WRAP_H__
#define __MKL_WRAP_H__
/* ========================================================================= */
#include "mkl_types.h"
#include "BOOL.h"
/* ========================================================================= */
BOOL      MKL_Installed_wrap(void);
void      MKL_Get_Version_wrap(MKLVersion *ver);
void      MKL_Get_Version_String_wrap(char * buffer, int len);
void      MKL_Free_Buffers_wrap(void);
void      MKL_Thread_Free_Buffers_wrap(void);
MKL_INT64 MKL_Mem_Stat_wrap(int* nbuffers); 
void      MKL_Get_Cpu_Clocks_wrap(unsigned MKL_INT64 *clocks);
double    MKL_Get_Cpu_Frequency_wrap(void);
void      MKL_Set_Cpu_Frequency_wrap(double* fq);
void      MKL_Set_Num_Threads_wrap(int nth);
int       MKL_Get_Max_Threads_wrap(void);
int       MKL_Domain_Set_Num_Threads_wrap(int nth, int MKL_DOMAIN);
int       MKL_Domain_Get_Max_Threads_wrap(int MKL_DOMAIN);
void      MKL_Set_Dynamic_wrap(int bool_MKL_DYNAMIC);
int       MKL_Get_Dynamic_wrap(void);
int       MKL_Enable_Instructions_wrap(int MKL_AVX_ENABLE);
/* ========================================================================= */
#endif /* __MKL_WRAP_H__ */
/* ========================================================================= */
