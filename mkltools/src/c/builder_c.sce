// Copyright 2010 - DIGITEO - Allan CORNET

src_c_path = get_absolute_file_path("builder_c.sce");

CFLAGS = "-I" + src_c_path;

files_src = "mkl_wrap.c";

tbx_build_src("mkl_wrap",         ..
              files_src,          ..
              "c",                ..
              src_c_path,         ..
              "",                 ..
              "",                 ..
              CFLAGS);

clear tbx_build_src;
clear src_c_path;
clear CFLAGS;
