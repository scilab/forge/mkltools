/* ========================================================================= */
// Copyright (C) 2010 - DIGITEO - Allan CORNET
/* ========================================================================= */
#include <windows.h>
#include <string.h>
#include "mkl_wrap.h"
/* ========================================================================= */
#define BLAS_LIBRARY_NAME                 "blasplus.dll"
#define LAPACK_LIBRARY_NAME               "lapack.dll"
#define MKL_GET_VERSION_NAME              "MKL_Get_Version"
#define MKL_GET_VERSION_STRING_NAME       "MKL_Get_Version_String"
#define MKL_FREE_BUFFERS_NAME             "MKL_Free_Buffers"
#define MKL_THREAD_FREE_BUFFERS_NAME      "MKL_Thread_Free_Buffers"
#define MKL_MEM_STAT_NAME                 "MKL_Mem_Stat"
#define MKL_GET_CPU_CLOCKS_NAME           "MKL_Get_Cpu_Clocks"
#define MKL_GET_CPU_FREQUENCY_NAME        "MKL_Get_Cpu_Frequency"
#define MKL_SET_CPU_FREQUENCY_NAME        "MKL_Set_Cpu_Frequency"
#define MKL_SET_NUM_THREADS_NAME          "MKL_Set_Num_Threads"
#define MKL_GET_MAX_THREADS_NAME          "MKL_Get_Max_Threads"
#define MKL_DOMAIN_SET_NUM_THREADS_NAME   "MKL_Domain_Set_Num_Threads"
#define MKL_DOMAIN_GET_MAX_THREADS_NAME   "MKL_Domain_Get_Max_Threads"
#define MKL_SET_DYNAMIC_NAME              "MKL_Set_Dynamic"
#define MKL_GET_DYNAMIC_NAME              "MKL_Get_Dynamic"
#define MKL_ENABLE_INSTRUCTIONS_NAME      "MKL_Enable_Instructions"
/* ========================================================================= */
typedef void      (*MKL_Get_Version_BLAS_PROC)(MKLVersion *ver);
typedef void      (*MKL_Get_Version_String_BLAS_PROC)(char * buffer, int len);
typedef void      (*MKL_Free_Buffers_BLAS_PROC)(void);
typedef void      (*MKL_Thread_Free_Buffers_BLAS_PROC)(void);
typedef MKL_INT64 (*MKL_Mem_Stat_BLAS_PROC)(int* nbuffers); 
typedef void      (*MKL_Get_Cpu_Clocks_BLAS_PROC)(unsigned MKL_INT64 *);
typedef double    (*MKL_Get_Cpu_Frequency_BLAS_PROC)(void);
typedef void      (*MKL_Set_Cpu_Frequency_BLAS_PROC)(double *fq);
typedef void      (*MKL_Set_Num_Threads_BLAS_PROC)(int nth);
typedef int       (*MKL_Get_Max_Threads_BLAS_PROC)(void);
typedef int       (*MKL_Domain_Set_Num_Threads_BLAS_PROC)(int nth, int MKL_DOMAIN);
typedef int       (*MKL_Domain_Get_Max_Threads_BLAS_PROC)(int MKL_DOMAIN);
typedef void      (*MKL_Set_Dynamic_BLAS_PROC)(int bool_MKL_DYNAMIC);
typedef int       (*MKL_Get_Dynamic_BLAS_PROC)(void);
typedef int       (*MKL_Enable_Instructions_BLAS_PROC)(int);
/* ========================================================================= */
typedef void      (*MKL_Get_Version_LAPACK_PROC)(MKLVersion *ver);
typedef void      (*MKL_Get_Version_String_LAPACK_PROC)(char * buffer, int len);
typedef void      (*MKL_Free_Buffers_LAPACK_PROC)(void);
typedef void      (*MKL_Thread_Free_Buffers_LAPACK_PROC)(void);
typedef MKL_INT64 (*MKL_Mem_Stat_LAPACK_PROC)(int* nbuffers); 
typedef void      (*MKL_Get_Cpu_Clocks_LAPACK_PROC)(unsigned MKL_INT64 *);
typedef double    (*MKL_Get_Cpu_Frequency_LAPACK_PROC)(void);
typedef void      (*MKL_Set_Cpu_Frequency_LAPACK_PROC)(double *fq);
typedef void      (*MKL_Set_Num_Threads_LAPACK_PROC)(int nth);
typedef int       (*MKL_Get_Max_Threads_LAPACK_PROC)(void);
typedef int       (*MKL_Domain_Set_Num_Threads_LAPACK_PROC)(int nth, int MKL_DOMAIN);
typedef int       (*MKL_Domain_Get_Max_Threads_LAPACK_PROC)(int MKL_DOMAIN);
typedef void      (*MKL_Set_Dynamic_LAPACK_PROC)(int bool_MKL_DYNAMIC);
typedef int       (*MKL_Get_Dynamic_LAPACK_PROC)(void);
typedef int       (*MKL_Enable_Instructions_LAPACK_PROC)(int);
/* ========================================================================= */
static bIsInitialized = FALSE;
static HINSTANCE hBlasplusDll = NULL;
static HINSTANCE hLapackDll = NULL;
/* ========================================================================= */
static MKL_Get_Version_BLAS_PROC            MKL_Get_Version_BLAS_dynlib = NULL;
static MKL_Get_Version_String_BLAS_PROC     MKL_Get_Version_String_BLAS_dynlib = NULL;
static MKL_Free_Buffers_BLAS_PROC           MKL_Free_Buffers_BLAS_dynlib = NULL;
static MKL_Thread_Free_Buffers_BLAS_PROC    MKL_Thread_Free_Buffers_BLAS_dynlib = NULL;
static MKL_Mem_Stat_BLAS_PROC               MKL_Mem_Stat_BLAS_dynlib = NULL;
static MKL_Get_Cpu_Clocks_BLAS_PROC         MKL_Get_Cpu_Clocks_BLAS_dynlib = NULL;
static MKL_Get_Cpu_Frequency_BLAS_PROC      MKL_Get_Cpu_Frequency_BLAS_dynlib = NULL;
static MKL_Set_Cpu_Frequency_BLAS_PROC      MKL_Set_Cpu_Frequency_BLAS_dynlib = NULL;
static MKL_Set_Num_Threads_BLAS_PROC        MKL_Set_Num_Threads_BLAS_dynlib = NULL;
static MKL_Get_Max_Threads_BLAS_PROC        MKL_Get_Max_Threads_BLAS_dynlib = NULL;
static MKL_Domain_Set_Num_Threads_BLAS_PROC MKL_Domain_Set_Num_Threads_BLAS_dynlib = NULL;
static MKL_Domain_Get_Max_Threads_BLAS_PROC MKL_Domain_Get_Max_Threads_BLAS_dynlib = NULL;
static MKL_Set_Dynamic_BLAS_PROC            MKL_Set_Dynamic_BLAS_dynlib = NULL;
static MKL_Get_Dynamic_BLAS_PROC            MKL_Get_Dynamic_BLAS_dynlib = NULL;
static MKL_Enable_Instructions_BLAS_PROC    MKL_Enable_Instructions_BLAS_dynlib = NULL;
/* ========================================================================= */
static MKL_Get_Version_LAPACK_PROC            MKL_Get_Version_LAPACK_dynlib = NULL;
static MKL_Get_Version_String_LAPACK_PROC     MKL_Get_Version_String_LAPACK_dynlib = NULL;
static MKL_Free_Buffers_LAPACK_PROC           MKL_Free_Buffers_LAPACK_dynlib = NULL;
static MKL_Thread_Free_Buffers_LAPACK_PROC    MKL_Thread_Free_Buffers_LAPACK_dynlib = NULL;
static MKL_Mem_Stat_LAPACK_PROC               MKL_Mem_Stat_LAPACK_dynlib = NULL;
static MKL_Get_Cpu_Clocks_LAPACK_PROC         MKL_Get_Cpu_Clocks_LAPACK_dynlib = NULL;
static MKL_Get_Cpu_Frequency_LAPACK_PROC      MKL_Get_Cpu_Frequency_LAPACK_dynlib = NULL;
static MKL_Set_Cpu_Frequency_LAPACK_PROC      MKL_Set_Cpu_Frequency_LAPACK_dynlib = NULL;
static MKL_Set_Num_Threads_LAPACK_PROC        MKL_Set_Num_Threads_LAPACK_dynlib = NULL;
static MKL_Get_Max_Threads_LAPACK_PROC        MKL_Get_Max_Threads_LAPACK_dynlib = NULL;
static MKL_Domain_Set_Num_Threads_LAPACK_PROC MKL_Domain_Set_Num_Threads_LAPACK_dynlib = NULL;
static MKL_Domain_Get_Max_Threads_LAPACK_PROC MKL_Domain_Get_Max_Threads_LAPACK_dynlib = NULL;
static MKL_Set_Dynamic_LAPACK_PROC            MKL_Set_Dynamic_LAPACK_dynlib = NULL;
static MKL_Get_Dynamic_LAPACK_PROC            MKL_Get_Dynamic_LAPACK_dynlib = NULL;
static MKL_Enable_Instructions_LAPACK_PROC    MKL_Enable_Instructions_LAPACK_dynlib = NULL;
/* ========================================================================= */
static BOOL MKL_Initialize_wrap(void);
static BOOL MKL_Terminate_wrap(void);
/* ========================================================================= */
static BOOL MKL_Initialize_wrap(void)
{
    if (hBlasplusDll == NULL)
    {
        hBlasplusDll = LoadLibrary(BLAS_LIBRARY_NAME);
    }
    
    if (hLapackDll == NULL)
    {
        hLapackDll = LoadLibrary(LAPACK_LIBRARY_NAME);
    }
    
    if ( (hBlasplusDll == NULL) || (hLapackDll == NULL) ) 
    {
        if (hBlasplusDll) 
        {
            FreeLibrary(hBlasplusDll);
            hBlasplusDll = NULL;
        }
        
        if (hLapackDll)
        {
            FreeLibrary(hLapackDll);
            hLapackDll = NULL;
        }
        bIsInitialized = FALSE;
        return FALSE;
    }
    
    MKL_Get_Version_BLAS_dynlib = (MKL_Get_Version_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_GET_VERSION_NAME);
    MKL_Get_Version_String_BLAS_dynlib = (MKL_Get_Version_String_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_GET_VERSION_STRING_NAME);
    MKL_Free_Buffers_BLAS_dynlib = (MKL_Free_Buffers_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_FREE_BUFFERS_NAME);
    MKL_Thread_Free_Buffers_BLAS_dynlib = (MKL_Thread_Free_Buffers_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_THREAD_FREE_BUFFERS_NAME);
    MKL_Mem_Stat_BLAS_dynlib = (MKL_Mem_Stat_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_MEM_STAT_NAME);
    MKL_Get_Cpu_Clocks_BLAS_dynlib = (MKL_Get_Cpu_Clocks_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_GET_CPU_CLOCKS_NAME);
    MKL_Get_Cpu_Frequency_BLAS_dynlib = (MKL_Get_Cpu_Frequency_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_GET_CPU_FREQUENCY_NAME);
    MKL_Set_Cpu_Frequency_BLAS_dynlib = (MKL_Set_Cpu_Frequency_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_SET_CPU_FREQUENCY_NAME);
    MKL_Set_Num_Threads_BLAS_dynlib = (MKL_Set_Num_Threads_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_SET_NUM_THREADS_NAME);
    MKL_Get_Max_Threads_BLAS_dynlib = (MKL_Get_Max_Threads_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_GET_MAX_THREADS_NAME);
    MKL_Domain_Set_Num_Threads_BLAS_dynlib = (MKL_Domain_Set_Num_Threads_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_DOMAIN_SET_NUM_THREADS_NAME);
    MKL_Domain_Get_Max_Threads_BLAS_dynlib = (MKL_Domain_Get_Max_Threads_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_DOMAIN_GET_MAX_THREADS_NAME);
    MKL_Set_Dynamic_BLAS_dynlib = (MKL_Set_Dynamic_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_SET_DYNAMIC_NAME);
    MKL_Get_Dynamic_BLAS_dynlib = (MKL_Get_Dynamic_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_GET_DYNAMIC_NAME);
    MKL_Enable_Instructions_BLAS_dynlib = (MKL_Enable_Instructions_BLAS_PROC)GetProcAddress(hBlasplusDll, MKL_ENABLE_INSTRUCTIONS_NAME);
    
    MKL_Get_Version_LAPACK_dynlib = (MKL_Get_Version_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_GET_VERSION_NAME);
    MKL_Get_Version_String_LAPACK_dynlib = (MKL_Get_Version_String_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_GET_VERSION_STRING_NAME);
    MKL_Free_Buffers_LAPACK_dynlib = (MKL_Free_Buffers_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_FREE_BUFFERS_NAME);
    MKL_Thread_Free_Buffers_LAPACK_dynlib = (MKL_Thread_Free_Buffers_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_THREAD_FREE_BUFFERS_NAME);
    MKL_Mem_Stat_LAPACK_dynlib = (MKL_Mem_Stat_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_MEM_STAT_NAME);
    MKL_Get_Cpu_Clocks_LAPACK_dynlib = (MKL_Get_Cpu_Clocks_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_GET_CPU_CLOCKS_NAME);
    MKL_Get_Cpu_Frequency_LAPACK_dynlib = (MKL_Get_Cpu_Frequency_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_GET_CPU_FREQUENCY_NAME);
    MKL_Set_Cpu_Frequency_LAPACK_dynlib = (MKL_Set_Cpu_Frequency_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_SET_CPU_FREQUENCY_NAME);
    MKL_Set_Num_Threads_LAPACK_dynlib = (MKL_Set_Num_Threads_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_SET_NUM_THREADS_NAME);
    MKL_Get_Max_Threads_LAPACK_dynlib = (MKL_Get_Max_Threads_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_GET_MAX_THREADS_NAME);
    MKL_Domain_Set_Num_Threads_LAPACK_dynlib = (MKL_Domain_Set_Num_Threads_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_DOMAIN_SET_NUM_THREADS_NAME);
    MKL_Domain_Get_Max_Threads_LAPACK_dynlib = (MKL_Domain_Get_Max_Threads_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_DOMAIN_GET_MAX_THREADS_NAME);
    MKL_Set_Dynamic_LAPACK_dynlib = (MKL_Set_Dynamic_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_SET_DYNAMIC_NAME);
    MKL_Get_Dynamic_LAPACK_dynlib = (MKL_Get_Dynamic_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_GET_DYNAMIC_NAME);
    MKL_Enable_Instructions_LAPACK_dynlib = (MKL_Enable_Instructions_LAPACK_PROC)GetProcAddress(hLapackDll, MKL_ENABLE_INSTRUCTIONS_NAME);
    
    if (MKL_Get_Version_BLAS_dynlib &&
        MKL_Get_Version_String_BLAS_dynlib &&
        MKL_Free_Buffers_BLAS_dynlib &&
        MKL_Thread_Free_Buffers_BLAS_dynlib &&
        MKL_Mem_Stat_BLAS_dynlib &&
        MKL_Get_Cpu_Clocks_BLAS_dynlib &&
        MKL_Get_Cpu_Frequency_BLAS_dynlib &&
        MKL_Set_Cpu_Frequency_BLAS_dynlib &&
        MKL_Set_Num_Threads_BLAS_dynlib &&
        MKL_Get_Max_Threads_BLAS_dynlib &&
        MKL_Domain_Set_Num_Threads_BLAS_dynlib &&
        MKL_Domain_Get_Max_Threads_BLAS_dynlib &&
        MKL_Set_Dynamic_BLAS_dynlib &&
        MKL_Get_Dynamic_BLAS_dynlib &&
        MKL_Enable_Instructions_BLAS_dynlib &&
        MKL_Get_Version_LAPACK_dynlib &&
        MKL_Get_Version_String_LAPACK_dynlib &&
        MKL_Free_Buffers_LAPACK_dynlib &&
        MKL_Thread_Free_Buffers_LAPACK_dynlib &&
        MKL_Mem_Stat_LAPACK_dynlib &&
        MKL_Get_Cpu_Clocks_LAPACK_dynlib &&
        MKL_Get_Cpu_Frequency_LAPACK_dynlib &&
        MKL_Set_Cpu_Frequency_LAPACK_dynlib &&
        MKL_Set_Num_Threads_LAPACK_dynlib &&
        MKL_Get_Max_Threads_LAPACK_dynlib &&
        MKL_Domain_Set_Num_Threads_LAPACK_dynlib &&
        MKL_Domain_Get_Max_Threads_LAPACK_dynlib &&
        MKL_Set_Dynamic_LAPACK_dynlib &&
        MKL_Get_Dynamic_LAPACK_dynlib &&
        MKL_Enable_Instructions_LAPACK_dynlib)
    {
        bIsInitialized = TRUE;
        return TRUE;
    }
    
    MKL_Terminate_wrap();
    bIsInitialized = FALSE;
    return FALSE;
}
/* ========================================================================= */
static BOOL MKL_Terminate_wrap(void)
{
    if (hBlasplusDll) 
    {
        FreeLibrary(hBlasplusDll);
        hBlasplusDll = NULL;
    }
    
    if (hLapackDll)
    {
        FreeLibrary(hLapackDll);
        hLapackDll = NULL;
    }
    
    MKL_Get_Version_BLAS_dynlib = NULL;
    MKL_Get_Version_String_BLAS_dynlib = NULL;
    MKL_Free_Buffers_BLAS_dynlib = NULL;
    MKL_Thread_Free_Buffers_BLAS_dynlib = NULL;
    MKL_Mem_Stat_BLAS_dynlib = NULL;
    MKL_Get_Cpu_Clocks_BLAS_dynlib = NULL;
    MKL_Get_Cpu_Frequency_BLAS_dynlib = NULL;
    MKL_Set_Cpu_Frequency_BLAS_dynlib = NULL;
    MKL_Set_Num_Threads_BLAS_dynlib = NULL;
    MKL_Get_Max_Threads_BLAS_dynlib = NULL;
    MKL_Domain_Set_Num_Threads_BLAS_dynlib = NULL;
    MKL_Domain_Get_Max_Threads_BLAS_dynlib = NULL;
    MKL_Set_Dynamic_BLAS_dynlib = NULL;
    MKL_Get_Dynamic_BLAS_dynlib = NULL;
    MKL_Enable_Instructions_BLAS_dynlib = NULL;
    MKL_Get_Version_LAPACK_dynlib = NULL;
    MKL_Get_Version_String_LAPACK_dynlib = NULL;
    MKL_Free_Buffers_LAPACK_dynlib = NULL;
    MKL_Thread_Free_Buffers_LAPACK_dynlib = NULL;
    MKL_Mem_Stat_LAPACK_dynlib = NULL;
    MKL_Get_Cpu_Clocks_LAPACK_dynlib = NULL;
    MKL_Get_Cpu_Frequency_LAPACK_dynlib = NULL;
    MKL_Set_Cpu_Frequency_LAPACK_dynlib = NULL;
    MKL_Set_Num_Threads_LAPACK_dynlib = NULL;
    MKL_Get_Max_Threads_LAPACK_dynlib = NULL;
    MKL_Domain_Set_Num_Threads_LAPACK_dynlib = NULL;
    MKL_Domain_Get_Max_Threads_LAPACK_dynlib = NULL;
    MKL_Set_Dynamic_LAPACK_dynlib = NULL;
    MKL_Get_Dynamic_LAPACK_dynlib = NULL;
    MKL_Enable_Instructions_LAPACK_dynlib = NULL;
    
    bIsInitialized = FALSE;
    
    return TRUE;
}
/* ========================================================================= */
BOOL      MKL_Installed_wrap(void)
{
    if (!bIsInitialized)
    {
        return MKL_Initialize_wrap();
    }
    return TRUE;
}
/* ========================================================================= */
void      MKL_Get_Version_wrap(MKLVersion *ver)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        (MKL_Get_Version_BLAS_dynlib)(ver);
    }
}
/* ========================================================================= */
void      MKL_Get_Version_String_wrap(char * buffer, int len)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        (MKL_Get_Version_String_BLAS_dynlib)(buffer, len);
    }
}
/* ========================================================================= */
void      MKL_Free_Buffers_wrap(void)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        (MKL_Free_Buffers_LAPACK_dynlib)();
        (MKL_Free_Buffers_BLAS_dynlib)();
    }
}
/* ========================================================================= */
void      MKL_Thread_Free_Buffers_wrap(void)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        (MKL_Thread_Free_Buffers_LAPACK_dynlib)();
        (MKL_Thread_Free_Buffers_BLAS_dynlib)();
    }
}
/* ========================================================================= */
MKL_INT64 MKL_Mem_Stat_wrap(int* nbuffers)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        return (MKL_Mem_Stat_BLAS_dynlib )(nbuffers);
    }
    return 0;
}
/* ========================================================================= */
void      MKL_Get_Cpu_Clocks_wrap(unsigned MKL_INT64 *clocks)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        (MKL_Get_Cpu_Clocks_BLAS_dynlib)(clocks);
    }
}
/* ========================================================================= */
double    MKL_Get_Cpu_Frequency_wrap(void)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        return (MKL_Get_Cpu_Frequency_BLAS_dynlib)();
    }
    return 0.0;
}
/* ========================================================================= */
void      MKL_Set_Cpu_Frequency_wrap(double *fq)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        (MKL_Set_Cpu_Frequency_LAPACK_dynlib)(fq);
        (MKL_Set_Cpu_Frequency_BLAS_dynlib)(fq);
    }
}
/* ========================================================================= */
void      MKL_Set_Num_Threads_wrap(int nth)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        (MKL_Set_Num_Threads_LAPACK_dynlib)(nth);
        (MKL_Set_Num_Threads_BLAS_dynlib)(nth);
    }
}
/* ========================================================================= */
int       MKL_Get_Max_Threads_wrap(void)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        return (MKL_Get_Max_Threads_BLAS_dynlib)();
    }
    return 0;
}
/* ========================================================================= */
int       MKL_Domain_Set_Num_Threads_wrap(int nth, int MKL_DOMAIN)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        (MKL_Domain_Set_Num_Threads_LAPACK_dynlib)(nth, MKL_DOMAIN);
        return (MKL_Domain_Set_Num_Threads_BLAS_dynlib)(nth, MKL_DOMAIN);
    }
    return 0;
}
/* ========================================================================= */
int       MKL_Domain_Get_Max_Threads_wrap(int MKL_DOMAIN)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        return (MKL_Domain_Get_Max_Threads_BLAS_dynlib)(MKL_DOMAIN);
    }
    return 0;
}
/* ========================================================================= */
void      MKL_Set_Dynamic_wrap(int bool_MKL_DYNAMIC)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        (MKL_Set_Dynamic_LAPACK_dynlib)(bool_MKL_DYNAMIC);
        (MKL_Set_Dynamic_BLAS_dynlib)(bool_MKL_DYNAMIC);
    }
}
/* ========================================================================= */
int       MKL_Get_Dynamic_wrap(void)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        return (MKL_Get_Dynamic_BLAS_dynlib)();
    }
    return 0;
}
/* ========================================================================= */
int       MKL_Enable_Instructions_wrap(int MKL_AVX_ENABLE)
{
    if (!bIsInitialized) MKL_Initialize_wrap();
    if (bIsInitialized)
    {
        (MKL_Enable_Instructions_LAPACK_dynlib)(MKL_AVX_ENABLE);
        return (MKL_Enable_Instructions_BLAS_dynlib)(MKL_AVX_ENABLE);
    }
    return 0;
}
/* ========================================================================= */
