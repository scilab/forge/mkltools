// Copyrigth 2010 - DIGITEO - Allan CORNET

src_dir = get_absolute_file_path("cleaner_src.sce");

for language = ["c"]
    cleaner_file = src_dir + filesep() + language + filesep() + "cleaner.sce";
    if isfile(cleaner_file) then
        exec(cleaner_file);
        mdelete(cleaner_file);
    end
end

clear src_dir;
