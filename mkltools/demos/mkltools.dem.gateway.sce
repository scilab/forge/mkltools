
// Copyright (C) 2010 - DIGITEO - Allan CORNET
//

demopath = get_absolute_file_path("mkltools.dem.gateway.sce");
subdemolist = ["MKL Tools"             ,"mkltools.dem.sce"];
subdemolist(:,2) = demopath + subdemolist(:,2);
clear demopath;
