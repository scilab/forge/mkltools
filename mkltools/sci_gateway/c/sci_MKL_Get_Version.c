/* ========================================================================== */
/* Allan CORNET */
/* DIGITEO 2010 */
/* ========================================================================== */
#include <string.h>
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "localization.h"
#include "mkl_wrap.h"
/* ========================================================================== */
int sci_MKL_Get_Version(char *fname)
{
    MKLVersion ver;

    Rhs = max(Rhs, 0);

    CheckRhs(0, 0);
    CheckLhs(1, 7);


    if (!MKL_Installed_wrap())
    {
        Scierror(999,_("%s: MKL not detected.\n"), fname);
        return 0;
    }

    MKL_Get_Version_wrap(&ver);

    if (Lhs >= 1)
    {
        createScalarDouble(pvApiCtx, Rhs + 1, (double)ver.MajorVersion);
        LhsVar(1) = Rhs + 1;
    }

    if (Lhs >= 2)
    {
        createScalarDouble(pvApiCtx, Rhs + 2, (double)ver.MinorVersion);
        LhsVar(2) = Rhs + 2;
    }

    if (Lhs >= 3)
    {
        createScalarDouble(pvApiCtx, Rhs + 3, (double)ver.UpdateVersion);
        LhsVar(3) = Rhs + 3;
    }

    if (Lhs >= 4)
    {
        createSingleString(pvApiCtx, Rhs + 4, ver.ProductStatus);
        LhsVar(4) = Rhs + 4;
    }

    if (Lhs >= 5)
    {
        createSingleString(pvApiCtx, Rhs + 5, ver.Build);
        LhsVar(5) = Rhs + 5;
    }

    if (Lhs >= 6)
    {
        createSingleString(pvApiCtx, Rhs + 6, ver.Processor);
        LhsVar(6) = Rhs + 6;
    }

    if (Lhs >= 7)
    {
        createSingleString(pvApiCtx, Rhs + 7, ver.Platform);
        LhsVar(7) = Rhs + 7;
    }

    C2F(putlhsvar)();

    return 0;
}
/* ========================================================================== */
