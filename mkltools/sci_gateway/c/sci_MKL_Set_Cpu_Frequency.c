/* ========================================================================== */
/* Allan CORNET */
/* DIGITEO 2010 */
/* ========================================================================== */
#include "stack-c.h" 
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "localization.h"
#include "mkl_wrap.h"
/* ========================================================================== */
int sci_MKL_Set_Cpu_Frequency(char *fname)
{
    SciErr sciErr;
    int *piAddressVarOne = NULL;
    double dFrequency = 0.0;
    
    CheckRhs(1, 1);
    CheckLhs(1, 1);
    
    if (!MKL_Installed_wrap())
    {
        Scierror(999,_("%s: MKL not detected.\n"), fname);
        return 0;
    }
    
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }
    
    if ( !isDoubleType(pvApiCtx, piAddressVarOne) )
    {
        Scierror(999,"%s: Wrong type for input argument #%d: A scalar expected.\n", fname, 1);
        return 0;
    }
    
    if ( getScalarDouble(pvApiCtx, piAddressVarOne, &dFrequency) )
    {
        Scierror(999,"%s: Wrong size for input argument #%d: A scalar expected.\n", fname, 1);
        return 0;
    }
    
    if (dFrequency <= 0.)
    {
        dFrequency = MKL_Get_Cpu_Frequency_wrap();
    }
    
    MKL_Set_Cpu_Frequency_wrap(&dFrequency);
    
    LhsVar(1) = 0;
    C2F(putlhsvar)();
    
    return 0;
}
/* ========================================================================== */
