/* ========================================================================== */
/* Allan CORNET */
/* DIGITEO 2010 */
/* ========================================================================== */
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "localization.h"
#include "mkl_wrap.h"
/* ========================================================================== */
int sci_MKL_Get_Version_String(char *fname)
{
    char bufferVersion[bsiz];

    Rhs = max(Rhs, 0);

    CheckRhs(0, 0);
    CheckLhs(1, 1);

    if (!MKL_Installed_wrap())
    {
        Scierror(999,_("%s: MKL not detected.\n"), fname);
        return 0;
    }

    MKL_Get_Version_String_wrap(bufferVersion, bsiz);

    createSingleString(pvApiCtx, Rhs + 1, bufferVersion);
    LhsVar(1) = Rhs + 1;
    C2F(putlhsvar)();

    return 0;
}
/* ========================================================================== */
