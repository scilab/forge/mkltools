/* ========================================================================== */
/* Allan CORNET */
/* DIGITEO 2010 */
/* ========================================================================== */
#include "stack-c.h" 
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "localization.h"
#include "mkl_wrap.h"
/* ========================================================================== */
int sci_MKL_Set_Num_Threads(char *fname)
{
    SciErr sciErr;
    int *piAddressVarOne = NULL;
    double dVarOne = 0.0;
    int iVarOne = 0;
    
    CheckRhs(1, 1);
    CheckLhs(1, 1);
    
    if (!MKL_Installed_wrap())
    {
        Scierror(999,_("%s: MKL not detected.\n"), fname);
        return 0;
    }

    /* get Address of inputs */
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }    
    
    /* check input type */
    if ( !isDoubleType(pvApiCtx, piAddressVarOne) )
    {
        Scierror(999,"%s: Wrong type for input argument #%d: A scalar expected.\n", fname, 1);
        return 0;
    }    

    if ( getScalarDouble(pvApiCtx, piAddressVarOne, &dVarOne) )
    {
        Scierror(999,"%s: Wrong size for input argument #%d: A scalar expected.\n", fname, 1);
        return 0;
    }
    
    iVarOne = (int)dVarOne;
    if ((double)iVarOne != dVarOne)
    {
        Scierror(999,"%s: Wrong value for input argument #%d: A integer value expected.\n", fname, 1);
        return 0;
    }
    
    if (iVarOne <= 0)
    {
        Scierror(999,"%s: Wrong value for input argument #%d: > 0 expected.\n", fname, 1);
        return 0;
    }
    
    MKL_Set_Num_Threads_wrap(iVarOne);
    
    LhsVar(1) = 0;
    C2F(putlhsvar)();
    
    return 0;
}
/* ========================================================================== */
