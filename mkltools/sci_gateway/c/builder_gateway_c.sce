// Copyright 2010 - DIGITEO - Allan CORNET

function builder_gateway_c_mkl()

if getos() == "Windows" then
    // to manage long pathname
    includes_src_c = "-I""" + get_absolute_file_path("builder_gateway_c.sce") + "../../src/c""";
else
    includes_src_c = "-I" + get_absolute_file_path("builder_gateway_c.sce") + "../../src/c";
end

// PutLhsVar managed by user in sci_sum and in sci_sub
// if you do not this variable, PutLhsVar is added
// in gateway generated (default mode in scilab 4.x and 5.x)
WITHOUT_AUTO_PUTLHSVAR = %t;

functions_list = ["MKL_Dom_Get_Max_Threads"   , "sci_MKL_Domain_Get_Max_Threads"; ..
                  "MKL_Dom_Set_Num_Threads"   , "sci_MKL_Domain_Set_Num_Threads"; ..
                  "MKL_Enable_Instructions"   , "sci_MKL_Enable_Instructions"; ..
                  "MKL_Free_Buffers"          , "sci_MKL_Free_Buffers"; ..
                  "MKL_Get_Cpu_Clocks"        , "sci_MKL_Get_Cpu_Clocks"; ..
                  "MKL_Get_Cpu_Frequency"     , "sci_MKL_Get_Cpu_Frequency"; ..
                  "MKL_Get_Dynamic"           , "sci_MKL_Get_Dynamic"; ..
                  "MKL_Get_Max_Threads"       , "sci_MKL_Get_Max_Threads"; ..
                  "MKL_Get_Version"           , "sci_MKL_Get_Version"; ..
                  "MKL_Get_Version_String"    , "sci_MKL_Get_Version_String"; ..
                  "MKL_Installed"             , "sci_MKL_Installed"; ..
                  "MKL_Mem_Stat"              , "sci_MKL_Mem_Stat"; ..
                  "MKL_Set_Cpu_Frequency"     , "sci_MKL_Set_Cpu_Frequency"; ..
                  "MKL_Set_Dynamic"           , "sci_MKL_Set_Dynamic"; ..
                  "MKL_Set_Num_Threads"       , "sci_MKL_Set_Num_Threads"; ..
                  "MKL_Thread_Free_Buffers"   , "sci_MKL_Thread_Free_Buffers"];


files_list = ["sci_MKL_Domain_Get_Max_Threads.c", ..
              "sci_MKL_Domain_Set_Num_Threads.c", ..
              "sci_MKL_Enable_Instructions.c", ..
              "sci_MKL_Free_Buffers.c", ..
              "sci_MKL_Get_Cpu_Clocks.c", ..
              "sci_MKL_Get_Cpu_Frequency.c", ..
              "sci_MKL_Get_Dynamic.c", ..
              "sci_MKL_Get_Max_Threads.c", ..
              "sci_MKL_Get_Version.c", ..
              "sci_MKL_Get_Version_String.c", ..
              "sci_MKL_Installed.c", ..
              "sci_MKL_Mem_Stat.c", ..
              "sci_MKL_Set_Cpu_Frequency.c", ..
              "sci_MKL_Set_Dynamic.c", ..
              "sci_MKL_Set_Num_Threads.c", ..
              "sci_MKL_Thread_Free_Buffers.c"];

tbx_build_gateway("mkltools", ..
                  functions_list, ..
                  files_list, ..
                  get_absolute_file_path("builder_gateway_c.sce"), ..
                  ["../../src/c/libmkl_wrap"], ..
                  "", ..
                  includes_src_c);

endfunction

builder_gateway_c_mkl();
clear builder_gateway_c_mkl;
