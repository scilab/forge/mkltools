/* ========================================================================== */
/* Allan CORNET */
/* DIGITEO 2010 */
/* ========================================================================== */
#include "stack-c.h" 
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "localization.h"
#include "mkl_wrap.h"
/* ========================================================================== */
int sci_MKL_Mem_Stat(char *fname)
{
    MKL_INT64 allocated_bytes;
    int       allocated_buffers;
    
    Rhs = max(Rhs, 0);
    
    CheckRhs(0, 0);
    CheckLhs(1, 2);
    
    if (!MKL_Installed_wrap())
    {
        Scierror(999,_("%s: MKL not detected.\n"), fname);
        return 0;
    }
    
    allocated_bytes = MKL_Mem_Stat_wrap(&allocated_buffers);
    
    if (Lhs >= 1)
    {
        createScalarDouble(pvApiCtx, Rhs + 1, (double) allocated_bytes);
        LhsVar(1) = Rhs + 1;
    }

    if (Lhs >= 2)
    {
        createScalarDouble(pvApiCtx, Rhs + 2, (double) allocated_buffers);
        LhsVar(2) = Rhs + 2;
    }

    C2F(putlhsvar)();
    
    return 0;
}
/* ========================================================================== */
