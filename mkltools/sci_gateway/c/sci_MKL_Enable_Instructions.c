/* ========================================================================== */
/* Allan CORNET */
/* DIGITEO 2010 */
/* ========================================================================== */
#include "stack-c.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "localization.h"
#include "mkl_wrap.h"
/* ========================================================================== */
int sci_MKL_Enable_Instructions(char *fname)
{
    SciErr sciErr;
    int *piAddressVarOne = NULL;

    CheckRhs(1, 1);
    CheckLhs(1, 1);

    if (!MKL_Installed_wrap())
    {
        Scierror(999,_("%s: MKL not detected.\n"), fname);
        return 0;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }

    if (isDoubleType(pvApiCtx, piAddressVarOne))
    {
        double dValue = 0.;
        int iValue = 0;
        int iErr = 0;

        if (!isScalar(pvApiCtx, piAddressVarOne))
        {
            Scierror(999,_("%s: Wrong size for input argument #%d: A scalar expected.\n"), fname, 1);
            return 0;
        }

        getScalarDouble(pvApiCtx, piAddressVarOne, &dValue);
        iValue = (int)dValue;

        if ((double)iValue != dValue)
        {
            Scierror(999,_("%s: Wrong value for input argument #%d: A integer value expected.\n"), fname, 1);
            return 0;
        }

        if ((iValue != 0) && (iValue != 1))
        {
            Scierror(999,_("%s: Wrong value for input argument #%d: 0 or 1 expected.\n"), fname, 1);
            return 0;
        }

        iErr = MKL_Enable_Instructions_wrap(iValue);
        createScalarBoolean(pvApiCtx, Rhs + 1, iErr);
        LhsVar(1) = Rhs + 1;

        C2F(putlhsvar)();
    }
    else
    {
        Scierror(999,_("%s: Wrong type for input argument #%d: An integer value expected.\n"), fname, 1);
        return 0;
    }

    return 0;
}
/* ========================================================================== */
