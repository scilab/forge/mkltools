/* ========================================================================== */
/* Allan CORNET */
/* DIGITEO 2010 */
/* ========================================================================== */
#include "stack-c.h" 
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "localization.h"
#include "mkl_wrap.h"
/* ========================================================================== */
int sci_MKL_Set_Dynamic(char *fname)
{
    SciErr sciErr;
    int *piAddressVarOne = NULL;

    CheckRhs(1, 1);
    CheckLhs(1, 1);
    
    if (!MKL_Installed_wrap())
    {
        Scierror(999,_("%s: MKL not detected.\n"), fname);
        return 0;
    }
    
    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 0;
    }
    
    if (isBooleanType(pvApiCtx, piAddressVarOne))
    {
        int bValue = 0;

        if (!isScalar(pvApiCtx, piAddressVarOne))
        {
            Scierror(999,_("%s: Wrong size for input argument #%d: A scalar expected.\n"), fname, 1);
            return 0;
        }

        getScalarBoolean(pvApiCtx, piAddressVarOne, &bValue);
        
        MKL_Set_Dynamic_wrap(bValue);
        
        LhsVar(1) = 0;
        C2F(putlhsvar)();
    }
    else
    {
        Scierror(999,_("%s: Wrong type for input argument #%d: A boolean value expected.\n"), fname, 1);
        return 0;
    }
    return 0;
}
/* ========================================================================== */
