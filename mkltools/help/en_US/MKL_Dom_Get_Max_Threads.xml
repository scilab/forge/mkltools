<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) DIGITEO - 2010 - Allan CORNET
 * 
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="MKL_Dom_Get_Max_Threads"
          xml:lang="en" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 2008-03-26 09:50:39 +0100 (mer., 26 mars 2008)
    $</pubdate>
  </info>

  <refnamediv>
    <refname>MKL_Dom_Get_Max_Threads</refname>

    <refpurpose>Inquires about the number of threads targeted for parallelism
    in different domains.</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>num = MKL_Dom_Get_Max_Threads(mask)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>mask</term>

        <listitem>
          <para>0 : MKL_ALL - another way to do what MKL_Get_Max_Threads
          does.</para>

          <para>1 : MKL_BLAS - BLAS domain</para>

          <para>2 : MKL_FFT - FFT domain (excluding cluster FFT)</para>

          <para>3 : MKL_VML - vector math library</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>num</term>

        <listitem>
          <para>Returns the hint about the number of threads for a given
          domain.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Inquires about the number of threads targeted for parallelism in
    different domains.</para>

    <para>This function allows the user of different domains of Intel MKL to
    inquire what number of threads is being used as a hint.</para>

    <para>The inquiry does not imply that this is the actual number of threads
    used. The number may vary depending on the value of the MKL_DYNAMIC
    variable and/or problem size, system resources, etc.</para>

    <para>But the function returns the value that MKL is targeting for a given
    domain.</para>

    <para>You are supposed to enter a valid domain.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">MKL_Dom_Get_Max_Threads(0)</programlisting>
  </refsection>

  <refsection>
    <title>Author</title>

    <simplelist type="vert">
      <member>Allan CORNET</member>

      <member>Intel MKL documentation</member>
    </simplelist>
  </refsection>
</refentry>
